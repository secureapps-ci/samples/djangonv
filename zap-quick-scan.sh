#!/usr/bin/env bash

target_url="${TARGET}"
results_file="${REPORT_FILE:-scan_result.txt}"

# if target is off-line, abort
curl -I "${target_url}" || exit

# run scanner
zap-cli \
    --verbose \
    quick-scan \
    --self-contained \
    --scanners all \
    --start-options '-config api.disablekey=true' \
    "${target_url}" \
    -l Informational | tee "${results_file}"

# collect hight/critical alerts
alerts=$(grep -c 'High|Critical' "${results_file}")
echo "$alerts"

yes

## verify results
#if [ "$alerts" -gt 0 ]; then
# echo "$alerts findings. Build failing"
# exit 1
#else
# exit 0
# echo "success - no findings identified"
#fi

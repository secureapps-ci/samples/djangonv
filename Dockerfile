# FROM python base image
FROM python:3.4-alpine

# files and workdir
RUN mkdir -p /app
COPY requirements.txt /app
COPY reset_db.sh /app
COPY run_app_docker.sh /app
COPY manage.py /app
COPY db.sqlite3 /app
COPY fixtures app/fixtures/
COPY taskManager /app/taskManager/
COPY ThreatDragonModels /app/ThreatDragonModels/

WORKDIR /app

RUN apk add --no-cache gawk sed bash grep bc coreutils
RUN pip install -r requirements.txt
RUN chmod +x reset_db.sh
RUN /app/reset_db.sh

# EXPOSE port 8000 for communication to/from server
EXPOSE 8000

# CMD specifcies the command to execute container starts running.
CMD ["/app/run_app_docker.sh"]
